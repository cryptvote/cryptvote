FROM node:lts-alpine AS react-builder
COPY frontend /app/frontend
WORKDIR /app/frontend
RUN npm install --silent
RUN npm run build

FROM golang:alpine AS go-builder
COPY --from=react-builder /app/frontend/build /app/frontend/build
COPY main.go /app/main.go
COPY internal /app/internal
COPY go.mod /app/go.mod
COPY go.sum /app/go.sum
WORKDIR /app
RUN CGO_ENABLED=0 go build -o cryptvote .

FROM alpine:latest
COPY --from=go-builder /app/cryptvote /app/cryptvote
EXPOSE 8080
CMD ["/app/cryptvote"]
