#!/bin/bash

cd frontend

npm install --silent
npm run build

cd ..

rm cryptvote
go build -o cryptvote .