import React, { useState, useEffect, createContext } from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";

import './i18n';
import './css/style.css';
import Layout from './partials/Layout'

import NotFound from './pages/NotFound';
import Home from './pages/Home';
import Register from './pages/Register';
import LogIn from './pages/LogIn';
import NewPoll from './pages/NewPoll';
import Poll from './pages/Poll';
import Profile from './pages/Profile';

export const AppContext = createContext();

export default function App() {
    

    const [instance, setInstance] = useState(localStorage.getItem('instance'));
    const [user, setUser] = useState(localStorage.getItem('user'));

    useEffect(() => {
        instance ? localStorage.setItem('instance', instance) : localStorage.removeItem('instance');
        user ? localStorage.setItem('user', user) : localStorage.removeItem('user');
    });

    function api(path, method, request) {
        let headers = { 'Content-Type': 'application/json' };

        try {
            headers['Authorization'] = 'Token ' + JSON.parse(localStorage.getItem('user')).token;
        } catch (e) {
            setUser(undefined);
        }

        let x = {
            method: method,
            headers: headers,
            body: JSON.stringify(request),
            mode: 'cors'
        }

        if (method === 'GET') x = {
            method: method,
            headers: headers,
            mode: 'cors'
        };

        return fetch(JSON.parse(localStorage.getItem('instance')).url + path, x).then(resp => resp.json());
    }

    return (
        <AppContext.Provider value={{ user, setUser, instance, setInstance, api }}>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout />}>
                        <Route index element={<Home />} />
                        <Route path="login" element={<LogIn />} />
                        <Route path="register" element={<Register />} />
                        <Route path="new" element={<NewPoll />} />
                        <Route path="poll" element={<Poll />} />
                        <Route path="poll/:id" element={<Poll />} />
                        <Route path="profile" element={<Profile />} />
                        <Route path="*" element={<NotFound />}></Route>
                    </Route>
                </Routes>
            </BrowserRouter>
        </AppContext.Provider>
    )
}
