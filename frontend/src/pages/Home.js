import React, { useContext } from 'react';
import { useTranslation } from "react-i18next";
import { Link, useNavigate } from 'react-router-dom';

import { AppContext } from "../App";
import Profile from './Profile';

export default function Home() {
    const appContext = useContext(AppContext);
    const [ t ] = useTranslation();

    if (!JSON.parse(appContext.instance) || !JSON.parse(appContext.user)) return (
        <article>
            <h1>{t('welcome')}!</h1>

            <p>{t('welcomeText')}</p>

            <div className="box">
                <center>
                <strong>{t('accountNeeded')}</strong><br />
                <Link to="/login"><button>{t('logIn')}</button></Link>
                &nbsp;
                <Link to="/register"><button>{t('register')}</button></Link>
                </center>
            </div>
        </article>
    )

    if (JSON.parse(appContext.user).emailConfirmed || !JSON.parse(appContext.instance).requireConfirmedMail) return (
        <article>
            <h1>{t('welcome')}, {JSON.parse(appContext.user).username}!</h1>
            <div className="box">
                <center>
                <Link to="/new"><button>{t('newPoll')}</button></Link>
                &nbsp;
                <Link to="/poll"><button>{t('viewPoll')}</button></Link>
                </center>
            </div>
        </article>
    )

    return <Profile />
}
