import { t } from "i18next";
import { useContext, useState, useEffect } from "react";
import { AppContext } from "../App";

export default function Profile() {
    const appContext = useContext(AppContext);
    const [email, setEmail] = useState(JSON.parse(appContext.user).email)

    const [oldPassword, setOldPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [confirmNewPassword, setConfirmNewPassword] = useState("");

    function send() {
        if (!newPassword && (newPassword !== confirmNewPassword)) {
            alert(t("passwordsDontMatch"));
        }

        appContext.api("/api/user", "POST", {
            "newEmail": email,
            "oldPassword": oldPassword,
            "newPassword": newPassword
        }).then(resp => {
            if (resp["success"]) {
                let user = JSON.parse(appContext.user);
                user.email = email;
                appContext.setUser(JSON.stringify(user));
                alert(t("saved"));
            }
            if (resp['error']) {
                return alert(t("error") + ": " + t(resp['error']));
            }
        });
    }

    return <article>
        <h1>{t("profile")}</h1>

        <label htmlFor="username">{t('username')}:</label><br />
        <input type="text" name="username" disabled={true} value={JSON.parse(appContext.user).username} /><br />
        <br />
        <h2>{t("email")}</h2>
        <label htmlFor="email" on>{t('email')}:</label><br />
        <input type="text" name="email" value={email} onChange={e => setEmail(e.target.value)} /><br />
        <br />
        <ConfirmEmail />
        <br />
        <h2>Jelszó megváltoztatása</h2>
        <label htmlFor="password">{t('oldPassword')}:</label><br />
        <input type="Password" name="oldPassword" onChange={e => setOldPassword(e.target.value)} /><br />
        <br />
        <label htmlFor="password">{t('newPassword')}:</label><br />
        <input type="Password" name="newPassword" onChange={e => setNewPassword(e.target.value)} /><br />
        <br />
        <label htmlFor="password">{t('confirmNewPassword')}:</label><br />
        <input type="Password" name="confirmNewPassword" onChange={e => setConfirmNewPassword(e.target.value)} /><br />

        <button onClick={() => send()}>{t('save')}</button>
    </article>
}

function ConfirmEmail() {
    const appContext = useContext(AppContext);

    const [codeSent, setCodeSent] = useState(false);
    const [confirmationCode, setConfirmationCode] = useState("");

    const [remaining, setRemaining] = useState(30);

    useEffect(() => {
        if (remaining > 0 && codeSent) setTimeout(() => { setRemaining(remaining - 1) }, 1000);
    });

    function sendCode() {
        appContext.api("/api/confirm", "GET", undefined).then(resp => {
            if (resp["success"]) {
                setCodeSent(true);
                setRemaining(30);
                return;
            }
            if (resp['error']) {
                return alert(t("error") + ": " + t(resp['error']));
            }
        })
    }

    function checkCode() {
        appContext.api("/api/confirm", "POST", { "code": confirmationCode }).then(resp => {
            if (resp['success']) {
                let user = JSON.parse(appContext.user);
                user.emailConfirmed = true;
                appContext.setUser(JSON.stringify(user));
                alert(t("saved"));
                return;
            }
            if (resp['error']) {
                return alert(t("error") + ": " + t(resp['error']));
            }
        })
    }

    if (JSON.parse(appContext.user).emailConfirmed) return t("emailIsConfirmed")

    if (codeSent) return <>
        <label htmlFor="confirmationCode">{t('confirmationCode')}:</label><br />
        <input type="text" name="confirmationCode" onChange={e => setConfirmationCode(e.target.value)} />
        &nbsp;
        <button onClick={() => checkCode()} >OK</button>
        <br />
        {remaining ? <span>{t("resendCode")}: {remaining} {t("second")}.</span> : <button onClick={() => sendCode()}>{t("resendCode")}</button>}
    </>

    return <>
        <span>{JSON.parse(appContext.instance).requireConfirmedMail ? <strong>{t("confirmRequired")}</strong> : t("emailNotConfirmed")}</span>&nbsp;
        &nbsp;
        <button onClick={() => sendCode()}>{t("sendCode")}</button>
    </>
}