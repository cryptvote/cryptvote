import React, { useState, useEffect, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams, useLocation, useNavigate } from 'react-router-dom';

import { AppContext } from "../App";
import NotFound from "./NotFound";
import { VoteBox } from '../partials/VoteBox';

import * as crypto from '../crypto';

export default function Poll() {
    const { t } = useTranslation();
    const appContext = useContext(AppContext);
    const navigate = useNavigate();

    let [poll, setPoll] = useState(false);

    const { id } = useParams();
    const { hash } = useLocation();

    const [code, setCode] = useState("");

    const [actualId, setActualId] = useState();
    const [secretKey, setSecretKey] = useState();

    useEffect(() => {
        if (id && hash) {
            setActualId(id);
            setSecretKey(hash.substring(1));
        }

        if (!poll && actualId && secretKey) {
            appContext.api("/api/poll?id=" + actualId, 'GET', undefined).then(resp => {
                if (resp['error']) return setPoll({ "id": 0 });

                setPoll({
                    "id": actualId,
                    "question": crypto.decrypt(resp.encryptedQuestion, secretKey),
                    "proof": crypto.decrypt(resp.encryptedProof, secretKey),
                    "expiry": new Date(resp.expiry * 1000),
                    "voted": resp.voted
                });
            });
        }
    });

    if (poll['id'] === 0) return <NotFound />

    if (poll) return <article>
        <center><h1>{poll.question}</h1></center>

        <div className="box">
            <center>
                <strong>{t("invite")}:</strong>
                <br />
                <input className="code" type="text" disabled={true} value={id + hash} />
                &nbsp;
                <button onClick={() => navigator.clipboard.writeText(id + hash)}>{t("copy")}</button>

            </center>
        </div>

        <div className="box">
            <center>
                <VoteBox poll={poll} secretKey={secretKey} />
            </center>
        </div>
    </article>

    return <article>
        <h1>{t("viewPoll")}</h1>

        <form onSubmit={(event) => {
            event.preventDefault();
            navigate("/poll/" + code);
            const x = code.split("#");
            setActualId(x[0]);
            setSecretKey(x[1]);
        }}>
            <label htmlFor="code">{t('enterCode')}:</label><br />
            <input type="text" className="code" name="code" onChange={e => setCode(e.target.value)}></input><br />
            <br />
            <input type="submit" value={t('viewPoll')} />
        </form>
    </article>
}
