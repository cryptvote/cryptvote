import { useState, useContext } from 'react';
import { useTranslation } from "react-i18next";
import { useNavigate } from 'react-router-dom';

import { AppContext } from "../App";
import InstanceSelector from '../partials/InstanceSelector';

export default function LogIn() {
    const { t } = useTranslation();
    const appContext = useContext(AppContext);
    const navigate = useNavigate();

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    function submit(event) {
        event.preventDefault();

        appContext.api('/api/login', 'POST', {
            "username": username,
            "password": password
        }).then(resp => {
            if (resp['error']) {
                alert(t("error") + ": " + t(resp['error']));
            } else {
                appContext.setUser(JSON.stringify(resp));
                navigate('/');
            }
        });
    };

    return (
        <article>
            <h1>{t('logIn')}</h1>

            <form onSubmit={submit}>
                <label htmlFor="instanceUrl">{t('instanceUrl')}:</label><br />
                <InstanceSelector /><br />
                <br />
                <label htmlFor="username">{t('username')}:</label><br />
                <input type="text" name="username" onChange={e => setUsername(e.target.value)}></input><br />
                <br />
                <label htmlFor="password">{t('password')}:</label><br />
                <input type="password" name="password" onChange={e => setPassword(e.target.value)} /><br />
                <br />
                <input type="submit" value={t('logIn')} />
            </form>
        </article>
    );
}
