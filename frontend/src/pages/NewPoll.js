import { useState, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';

import { DateSelector } from "../partials/DateSelector";
import { AppContext } from "../App";

import * as crypto from '../crypto';
import { t } from 'i18next';

export default function NewPoll() {
    const navigate = useNavigate();
    const { t } = useTranslation();
    const appContext = useContext(AppContext);

    const [question, setQuestion] = useState("");
    const [users, setUsers] = useState([]);
    const [invitedUsers] = useState([JSON.parse(appContext.user).id]);

    const [expiry, setExpiry] = useState((() => {
        const date = new Date();
        date.setDate(date.getDate() + 1);
        date.setHours(0);
        date.setMinutes(0);
        return date;
    })());

    if (users.length === 0) appContext.api('/api/user', 'GET', undefined).then(resp => setUsers(resp));

    async function submit(event) {
        event.preventDefault();

        const key = crypto.generateKey();
        const proof = crypto.generateProof();

        appContext.api('/api/poll', 'PUT', {
            "encryptedQuestion": crypto.encrypt(question, key),

            "encryptedProof": crypto.encrypt(proof, key),
            "expiry": Math.floor(expiry.getTime() / 1000),
            "users": invitedUsers
        }).then(resp => {
            if (resp['error']) {
                alert(t(resp['error']));
            } else if (resp['id']) {
                navigate('/poll/' + resp['id'] + '#' + key);
            }
        });
    }

    return (
        <article>
            <h1>{t('newPoll')}</h1>

            <form onSubmit={submit}>
                <label htmlFor="question">{t('question')}:</label><br />
                <input type="text" name="title" onChange={e => setQuestion(e.target.value)}></input><br />
                <br />
                <label htmlFor="expiry">{t('expiry')}:</label><br />
                <DateSelector date={expiry} /><br />
                <br />
                <label htmlFor="users">{t('invitedUsers')}:</label>
                <table className="users">
                    <tbody>

                        {users.map((user, index) =>
                            <User user={user} invitedUsers={invitedUsers} />
                        )}
                    </tbody>
                </table>
                <br />

                <input type="submit" value={t('newPoll')} onClick={submit} />
            </form>
        </article>
    );
}

function User(props) {
    const [user] = useState(props.user);
    const [invited, setInvited] = useState(false);

    const appContext = useContext(AppContext);
    if (props.user.id === JSON.parse(appContext.user).id) return

    return <tr className="user">
        <td><span>{user.username}</span></td>
        <td><button className={invited ? "red" : ""} onClick={(event) => {
            event.preventDefault();
            if (invited) {
                for (let i = 0; i < props.invitedUsers.length; i++) {
                    if (props.invitedUsers[i] === props.user.id) {
                        props.invitedUsers.splice(i, 1);
                    }
                }
                setInvited(false);
            } else {
                setInvited(true);
                props.invitedUsers.push(user.id);
            }

        }}>{invited ? t('remove') : t('add')}</button>
        </td>
    </tr>

}
