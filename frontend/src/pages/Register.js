import React, { useState, useContext } from 'react';
import { useTranslation } from "react-i18next";
import { useNavigate } from 'react-router-dom';

import InstanceSelector from '../partials/InstanceSelector';
import { AppContext } from "../App";

export default function Register() {
    const { t } = useTranslation();
    const appContext = useContext(AppContext);
    const navigate = useNavigate();

    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    function submit(event) {
        event.preventDefault();

        if (password !== confirmPassword) {
            return alert(t("error") + ": " + t("passwordsDontMatch"));
        }

        appContext.api('/api/user', 'PUT', {
            "username": username,
            "password": password,
            "email": email,
        }).then(resp => {
            if (resp['success']) {
                alert(t("registerSuccess"));
                navigate('/login');

                appContext.api('/api/login', 'POST', {
                    "username": username,
                    "password": password
                }).then(resp => {
                    if (resp['error']) {
                        alert(t("error") + ": " + t(resp['error']));
                    } else {
                        appContext.setUser(JSON.stringify(resp));
                        navigate('/');
                    }
                });
            } else if (resp['error']) {
                alert(t("error") + ": " + t(resp.error));
            } else {
                alert(t("error") + ": " + t("invalidResponse"));
            }
        });
    };

    return (
        <article>
            <h1>{t('register')}</h1>

            <form onSubmit={submit}>
                <label htmlFor="instanceUrl">{t('instanceUrl')}:</label><br />
                <InstanceSelector /><br />
                <br />
                <label htmlFor="email">{t('email')}:</label><br />
                <input type="text" name="email" onChange={e => setEmail(e.target.value)} /><br />
                <br />
                <label htmlFor="username">{t('username')}:</label><br />
                <input type="text" name="username" onChange={e => setUsername(e.target.value)}></input><br />
                <br />
                <label htmlFor="password">{t('password')}:</label><br />
                <input type="password" name="password" onChange={e => setPassword(e.target.value)} /><br />
                <br />
                <label htmlFor="confirmPassword">{t('confirmPassword')}:</label><br />
                <input type="password" name="confirmPassword" onChange={e => setConfirmPassword(e.target.value)} /><br />
                <br />
                <input type="submit" value={t('register')} />
            </form>
        </article>
    );
}