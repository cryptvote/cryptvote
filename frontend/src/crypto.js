import { secretbox, randomBytes } from "tweetnacl";
import * as utf8 from "@stablelib/utf8";
import * as base64 from "@stablelib/base64";

export function generateKey() {
    return base64.encode(randomBytes(secretbox.keyLength));
}

export function generateProof() {
    return base64.encode(randomBytes(secretbox.nonceLength));
}

export function encrypt(unencrypted, secretKey) {
    const key = base64.decode(secretKey);
    const nonce = randomBytes(secretbox.nonceLength);

    const box = secretbox(utf8.encode(unencrypted), nonce, key);

    const fullMessage = new Uint8Array(nonce.length + box.length);
    fullMessage.set(nonce);
    fullMessage.set(box, nonce.length);

    return base64.encode(fullMessage);
}

export function decrypt(encrypted, secretKey) {
    if (!encrypted) return "";

    const key = base64.decode(secretKey);

    const fullMessage = base64.decode(encrypted);
    const nonce = fullMessage.slice(0, secretbox.nonceLength);
    const message = fullMessage.slice(secretbox.nonceLength, fullMessage.length);

    const decrypted = secretbox.open(message, nonce, key);
    if (!decrypted) return "";
    return utf8.decode(decrypted);
}