import { useContext } from 'react';
import { Outlet, Link } from "react-router-dom";

import { AppContext } from "../App";

import NewSvg from '../svg/new.svg';
import LogOutSvg from '../svg/logout.svg';
import ProfileSvg from '../svg/profile.svg';

import { t } from 'i18next';

export default function Layout() {
    const appContext = useContext(AppContext);

    return (
        <>
            <header>
                <div className="inner">
                    <Link to="/"><h1 className="instanceName">{!appContext.instance ? "CryptVote" : JSON.parse(appContext.instance).name}</h1></Link>
                    {appContext.user ?
                        <nav>
                        {JSON.parse(appContext.user).emailConfirmed || !JSON.parse(appContext.instance).requireConfirmedMail ? 
                        <Link to="/new"><img alt={t("newPoll")} src={NewSvg} height="30" /></Link> : ""}
                        <Link to="/profile"><img alt={t("profile")} src={ProfileSvg} height="30" /></Link>
                        <Link to="/" onClick={() => {
                            appContext.setUser(false);
                        }}><img alt={t("logOut")} src={LogOutSvg} height="30" /></Link>
                        </nav>
                    : ""}
                </div>
            </header>

            <main className="inner">
                <Outlet />
            </main>

            <footer>
                <span>{t("footer")} - <a href="https://gitlab.com/cryptvote/cryptvote">{t("sourceCode")}</a></span>
            </footer>
        </>
    )
}