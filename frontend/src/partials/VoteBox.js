import React, { useState, useContext } from "react";
import { useTranslation } from "react-i18next";
import { AppContext } from "../App";
import * as crypto from '../crypto';


export function VoteBox(props) {
    const { t } = useTranslation();
    const appContext = useContext(AppContext);

    const [remaining, setRemaining] = useState(props.poll.expiry - Date.now());
    const [voted, setVoted] = useState(props.poll.voted);
    const [votes, setVotes] = useState(false);

    React.useEffect(() => {
        if (remaining > 0 || remaining === false) {
            setTimeout(() => {
                setRemaining(props.poll.expiry - Date.now())
            }, 1000);
        } else {
            setTimeout(() => appContext.api("/api/vote?id=" + props.poll.id, "GET", undefined).then(resp => {
                let vs = { "yes": 0, "no": 0, "abstain": 0, "invalid": 0 };
                let duplicates = [];

                if (resp !== null) {
                    for (let i = 0; i < resp.length; i++) {
                        let v = resp[i];

                        if (duplicates.includes(v.encryptedProof) || duplicates.includes(v.encryptedValue)) {
                            vs["invalid"]++;
                            continue;
                        }

                        duplicates.push(v.encryptedProof);
                        duplicates.push(v.encryptedValue);

                        if (crypto.decrypt(v.encryptedProof, props.secretKey) !== props.poll.proof) {
                            alert()
                            vs["invalid"]++;
                            continue;
                        }

                        const val = crypto.decrypt(v.encryptedValue, props.secretKey);
                        if (vs[val] !== undefined) {
                            vs[val]++;
                        }
                    }
                }

                setVotes(vs);
            }), 3000);
        }
    });

    const days = Math.floor(remaining / 1000 / 60 / 60 / 24);
    const hours = Math.floor((remaining / 1000 / 60 / 60 / 24 - days) * 24);
    const minutes = Math.floor(((remaining / 1000 / 60 / 60 / 24 - days) * 24 - hours) * 60);
    const seconds = Math.floor((((remaining / 1000 / 60 / 60 / 24 - days) * 24 - hours) * 60 - minutes) * 60);

    function vote(val) {
        appContext.api("/api/vote", 'PUT', {
            "poll": props.poll.id,
            "encryptedValue": crypto.encrypt(val, props.secretKey),
            "encryptedProof": crypto.encrypt(props.poll.proof, props.secretKey)
        }).then(resp => {
            if (resp['success']) {
                return setVoted(true);
            }

            if (resp['error']) {
                return alert(t(resp.error));
            }
        })
    }

    if (remaining > 0) return <>
        <strong>
            {t("remainingTime")}:
            <Counter unit={t("days")} value={days} />
            <Counter unit={t("hours")} value={hours} />
            <Counter unit={t("minutes")} value={minutes} />
            <Counter unit={t("seconds")} value={seconds} />
        </strong>
        <br />
        <button onClick={() => vote("yes")} className="green" >{t("yes")}</button>
        &nbsp;
        <button onClick={() => vote("no")} className="red" >{t("no")}</button>
        &nbsp;
        <button onClick={() => vote("abstain")}>{t("abstain")}</button>
        <br />
        <strong>{voted ? t("voted") : ""}</strong>
    </>

    if (votes) return <>
        <strong>{t("results")}:</strong><br />
        <span>{t("yes")} - {votes.yes}</span><br />
        <span>{t("no")} - {votes.no}</span><br />
        {votes.abstain ? <><span>{t("abstain")} - {votes.abstain}</span><br /></> : ""}
        {votes.invalid ? <span>{t("invalid")} - {votes.invalid}</span> : ""}
    </>

}

function Counter(params) {
    const unit = params.unit;
    const value = params.value;

    //if (value > 0)
    return <> {value} {unit}</>
    //else return <> </>
}