import React, { useState, useContext, useEffect, useRef } from 'react';
import { useTranslation } from "react-i18next";
import { AppContext } from "../App";

export default function InstanceSelector() {
    const { t } = useTranslation();
    const appContext = useContext(AppContext);

    const [instanceUrl, setInstanceUrl] = useState("");
    const [editing, setEditing] = useState(false);
    const [reachable, setReachable] = useState(false);

    const first = useRef(true);

    useEffect(() => {if (first.current) {
        checkInstanceUrl();
        first.current = false;
    }});
    
    function checkInstanceUrl() {
            // Correct instance URL


            let iu = instanceUrl;

            if (!iu) {
                if (JSON.parse(appContext.instance) && JSON.parse(appContext.instance).url) iu = JSON.parse(appContext.instance).url;
                else iu = window.location.host;
            }

            if (!(iu.startsWith("http://") || iu.startsWith("https://"))) {
                iu = "http://" + iu;
            }

            setInstanceUrl(iu);

            // Check if instance is reachable

            fetch(iu + "/api/instance", {
                method: "GET",
                headers: { 'Content-Type': 'application/json' },
                mode: 'cors'
            }).then(resp => resp.json().then(json => {

                if (json['cryptvote']) {
                    appContext.setInstance(JSON.stringify(json));
                    return setReachable(true);
                }
                appContext.setInstance(undefined);
                return setReachable(false);
            })).catch(e => {
                setReachable(false);
            });

    }


    if (editing) return (
        <>
            <input type="text" name="instance" value={instanceUrl} onChange={e => setInstanceUrl(e.target.value)} />
            &nbsp;
            <input type="button" value={t('OK')} onClick={() => {setEditing(false); checkInstanceUrl();}} />
        </>
    )

    return (
        <>
            <input type="text" name="instance" value={instanceUrl + " " + (reachable ? "✅" : "❌")} disabled />
            &nbsp;
            <input type="button" value={t('edit')} onClick={() => {setEditing(true); checkInstanceUrl();}} />
        </>
    )
}
