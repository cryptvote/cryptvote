# CryptVote

An encrypted e-voting system

## Installation

### Prerequisites

* You should have access to a GNU/Linux based server.
* For security, you should have access to an e-mail account you can use to send automated e-mails. (Optional)

### Using Docker & Docker Compose

Using Docker Compose on GNU/Linux is easiest and the recommended way of installating CryptVote in a production environment.

1. Follow the documentation available [here](https://docs.docker.com/engine/install/) to install Docker Engine and Docker Compose, assuming you don't have it already installed.
2. Clone the git repository: `git clone https://gitlab.com/cryptvote/cryptvote`
3. Navigate to the correct folder: `cd cryptvote`
4. Create a docker-compose.yml file and configure it to your likings. A sample file is included in the repository as `docker-compose.yml.example`.
5. Build the image and deploy the application: `docker-compose up -d`

## Manually

If you'd like to avoid containerization, you can simply build the applicatilon.

1. Install Node.js, Node Package Manager and Golang from your distro's repositories.
1. Clone the git repository: `git clone https://gitlab.com/cryptvote/cryptvote`
2. Navigate to the correct folder: `cd cryptvote`
3. Create a config.json file and configure it to your linkings. A sample file is included in the repository as `config.json.example`.
4. Build the application with the provided build script: `./build.sh`
5. Create a service to run the generated `cryptvote` binary.

### Reverse Proxy

It is recommended to put CryptVote behind an SSL terminating reverse proxy and use a valid HTTPS certificate.

[Nginx Proxy Manager](https://nginxproxymanager.com/) is a Docker-based solution that can be used for this task, but you may also configure a standard installation of Nginx.