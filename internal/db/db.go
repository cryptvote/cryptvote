package db

import (
	"database/sql"
	_ "embed"
	"fmt"
	"log"
	"strconv"
	"time"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/cryptvote/cryptvote/internal/config"
)

//go:embed init_mariadb.sql
var initMariaDb string

//go:embed init_sqlite.sql
var initSqlite string

var db *sql.DB

func Db() *sql.DB {

	if db == nil {
		// Connect to database

		var err error

		if config.Config().DbDriver == "sqlite3" {
			db, err = sql.Open(config.Config().DbDriver, "database.sqlite")
		} else {
			db, err = sql.Open(config.Config().DbDriver, config.Config().DbUsername+":"+config.Config().DbPassword+"@tcp("+config.Config().DbHost+":"+strconv.Itoa(config.Config().DbPort)+")/"+config.Config().DbName+"?multiStatements=true")
		}

		if err != nil {
			log.Fatal("Can't connect to database: " + err.Error())
		}

		// Check connection

		err = db.Ping()
		if err != nil {
			log.Fatal("Can't ping database: " + err.Error())
		}

		log.Print("Successfully connected to database.")

		// Initialize database

		if config.Config().DbDriver == "sqlite3" {
			_, err = db.Exec(string(initSqlite))
		} else {
			_, err = db.Exec(string(initMariaDb))
		}

		if err != nil {
			log.Print("Error while initalizing database: " + err.Error())
		}
	}

	return db
}

func Close() {
	db.Close()
}

// Sprintf:
// Parameters can't be used for column and table names and Go can't sanitize SQL strings -.-
// https://github.com/golang/go/issues/18478

func query(sel string, from string, where string, equals string) (string, error) {
	var row *sql.Row

	if where == "" && equals == "" {
		row = Db().QueryRow("SELECT %s FROM %s", sel, from)
	} else {
		row = Db().QueryRow(fmt.Sprintf("SELECT %s FROM %s WHERE %s = ?", sel, from, where), equals)
	}

	var result string
	err := row.Scan(&result)
	return result, err
}

func String(sel string, from string, where string, equals string, def string) string {
	result, err := query(sel, from, where, equals)
	if err != nil {
		return def
	}
	return result
}

func Bool(sel string, from string, where string, equals string, def bool) bool {
	strResult, err := query(sel, from, where, equals)
	if err != nil {
		return def
	}

	result, err := strconv.ParseBool(strResult)
	if err != nil {
		return def
	}
	return result
}

func Int(sel string, from string, where string, equals string, def int) int {
	strResult, err := query(sel, from, where, equals)
	if err != nil {
		return def
	}

	result, err := strconv.Atoi(strResult)
	if err != nil {
		return def
	}
	return result
}

func Time(sel string, from string, where string, equals string, def time.Time) time.Time {
	strResult, err := query(sel, from, where, equals)
	if err != nil {
		return def
	}

	intResult, err := strconv.ParseInt(strResult, 10, 64)
	if err != nil {
		return def
	}

	return time.Unix(intResult, 0)
}

func Delete(id int, table string) {
	_, err := Db().Exec(fmt.Sprintf("DELETE ? FROM %s", table), id)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func Update(update string, set string, to string, where string, equals string) {
	_, err := Db().Exec(fmt.Sprintf("UPDATE %s SET %s = ? WHERE %s = ?", update, set, where), to, equals)
	if err != nil {
		fmt.Println(err.Error())
	}
}
