CREATE TABLE IF NOT EXISTS user (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    email TINYTEXT UNIQUE,
    email_confirmed INTEGER,
    confirmation_code TINYTEXT,
    username varchar(32) NOT NULL UNIQUE,
    username_lowercase varchar(32) NOT NULL UNIQUE,
    password_hash TINYTEXT NOT NULL);

CREATE TABLE IF NOT EXISTS token (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    token TINYTEXT,
    user INTEGER NOT NULL);

CREATE TABLE IF NOT EXISTS poll (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    poll TINYTEXT,
    author INTEGER,
    expiry BIGINT,
    encrypted_question TEXT,
    encrypted_proof TEXT,
    users TEXT);

CREATE TABLE IF NOT EXISTS vote (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    poll TINYTEXT,
    encrypted_proof TEXT,
    encrypted_value TEXT);

CREATE TABLE IF NOT EXISTS voted (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    poll INTEGER NOT NULL,
    user INTEGER NOT NULL);

CREATE TABLE IF NOT EXISTS settings (
    db_version INTEGER);

INSERT IGNORE INTO settings VALUES (1);