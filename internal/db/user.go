package db

import (
	"log"
	"strconv"
	"strings"
)

type User int

func CreateUser(email string, email_confirmed int, username string, passwordHash string) {
	_, err := Db().Exec("INSERT INTO user(email, email_confirmed, username, username_lowercase, password_hash) VALUES(?, ?, ?, ?, ?)",
		email,
		email_confirmed,
		username,
		strings.ToLower(username),
		passwordHash)
	if err != nil {
		log.Println(err.Error())
	}
}

func FetchUser(username string) User {
	return User(Int("id", "user", "username_lowercase", strings.ToLower(username), 0))
}

func (u User) Username() string {
	return String("username", "user", "id", strconv.Itoa(int(u)), "Anonymous")
}

func (u User) PasswordHash() string {
	return String("password_hash", "user", "id", strconv.Itoa(int(u)), "")
}

func (u User) SetPasswordHash(passwordHash string) {
	Update("user", "password_hash", passwordHash, "id", strconv.Itoa(int(u)))
}

func (u User) DisplayName() string {
	return String("display_name", "user", "id", strconv.Itoa(int(u)), u.Username())
}

func (u User) Email() string {
	return String("email", "user", "id", strconv.Itoa(int(u)), "")
}

func (u User) SetEmail(email string) {
	Update("user", "email", email, "id", strconv.Itoa(int(u)))
}

func (u User) EmailConfirmed() bool {
	return Bool("email_confirmed", "user", "id", strconv.Itoa(int(u)), false)
}

func (u User) SetEmailConfirmed(confirmed bool) {
	str := "0"
	if confirmed {
		str = "1"
	}

	Update("user", "email_confirmed", str, "id", strconv.Itoa(int(u)))
}

func (u User) ConfirmationCode() string {
	return String("confirmation_code", "user", "id", strconv.Itoa(int(u)), "")
}

func (u User) SetConfirmationCode(code string) {
	Update("user", "confirmation_code", code, "id", strconv.Itoa(int(u)))
}

func (u User) SetDisplayName(displayName string) {
	Update("user", "display_name", displayName, "id", strconv.Itoa(int(u)))
}

func (u User) Delete() {
	Delete(int(u), "user")
}

func (u User) IsInvited(poll Poll) bool {
	for _, user := range poll.InvitedUsers() {
		if user == u {
			return true
		}
	}
	return false
}

func (u User) HasVoted(poll Poll) bool {
	rows, err := db.Query("SELECT id FROM voted WHERE poll = ? AND user = ?", string(poll), int(u))
	defer rows.Close()
	if err == nil && rows.Next() {
		return true
	}
	return false
}

func (u User) CastVote(poll Poll) {
	_, err := Db().Exec("INSERT INTO voted(user, poll) VALUES(?, ?)", int(u), string(poll))
	if err != nil {
		log.Println(err.Error())
	}
}

func AllUsers() []User {
	var users []User

	rows, err := Db().Query("SELECT id FROM user")
	if err != nil {
		log.Println(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var user User
		rows.Scan(&user)
		users = append(users, user)
	}

	return users
}
