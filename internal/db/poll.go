package db

import (
	"encoding/json"
	"log"
	"math/rand"
	"strconv"
	"time"
)

type Poll int

func generateId() string {
	rand.Seed(time.Now().UnixNano())
	letters := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

	s := make([]rune, 8)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

func FetchPoll(poll string) Poll {
	return Poll(Int("id", "poll", "poll", poll, 0))
}

func (p Poll) Poll() string {
	return String("poll", "poll", "id", strconv.Itoa(int(p)), "")
}
func CreatePoll(author User, expiry int64, encryptedQuestion string, encryptedProof string, users []int) Poll {
	poll := generateId()

	usersJson, err := json.Marshal(users)
	if err != nil {
		log.Println(err.Error())
	}

	_, err = Db().Exec("INSERT INTO poll(poll, author, expiry, encrypted_question, encrypted_proof, users) VALUES (?, ?, ?, ?, ?, ?)",
		poll,
		int(author),
		expiry,
		encryptedQuestion,
		encryptedProof,
		string(usersJson[:]),
	)
	if err != nil {
		log.Println(err.Error())
	}

	return FetchPoll(poll)
}

func (p Poll) Author() User {
	return User(Int("author", "poll", "id", strconv.Itoa(int(p)), 0))
}

func (p Poll) Expiry() time.Time {
	return Time("expiry", "poll", "id", strconv.Itoa(int(p)), time.Now())
}

func (p Poll) Expired() bool {
	if !p.Expiry().Before(time.Now()) {
		return false
	}
	return true
}

func (p Poll) EncryptedQuestion() string {
	return String("encrypted_question", "poll", "id", strconv.Itoa(int(p)), "")
}

func (p Poll) EncryptedProof() string {
	return String("encrypted_proof", "poll", "id", strconv.Itoa(int(p)), "")
}

func (p Poll) InvitedUsers() []User {
	var users []User

	var ints []int
	json.Unmarshal([]byte(String("users", "poll", "id", strconv.Itoa(int(p)), "")), &ints)

	for _, i := range ints {
		user := User(i)
		if user.Username() != "" {
			users = append(users, user)
		}
	}

	return users
}

func (p Poll) Votes() []Vote {
	var votes []Vote

	rows, err := Db().Query("SELECT id FROM vote WHERE poll = ?", strconv.Itoa(int(p)))
	if err != nil {
		log.Println(err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var vote Vote
		rows.Scan(&vote)
		votes = append(votes, vote)
	}

	return votes
}
