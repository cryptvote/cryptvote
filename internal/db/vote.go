package db

import (
	"log"
	"strconv"
)

type Vote int

func CreateVote(poll Poll, encryptedProof string, encryptedValue string) {
	_, err := Db().Exec("INSERT INTO vote(poll, encrypted_proof, encrypted_value) VALUES(?, ?, ?)", poll, encryptedProof, encryptedValue)
	if err != nil {
		log.Println(err.Error())
	}
}

func (v Vote) EncryptedProof() string {
	return String("encrypted_proof", "vote", "id", strconv.Itoa(int(v)), "")
}

func (v Vote) EncryptedValue() string {
	return String("encrypted_value", "vote", "id", strconv.Itoa(int(v)), "")
}
