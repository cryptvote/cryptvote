package db

import (
	"crypto/rand"
	"encoding/hex"
	"log"
	"strconv"
	"strings"
)

type Token int

func (t Token) Token() string {
	return String("token", "token", "id", strconv.Itoa(int(t)), "")
}

func FetchToken(token string) Token {
	return Token(Int("id", "token", "token", strings.ToLower(token), 0))
}

func CreateToken(user User) Token {
	random := make([]byte, 16)

	_, err := rand.Read(random)
	if err != nil {
		log.Fatal(err)
	}

	token := hex.EncodeToString(random)

	_, err = Db().Exec("INSERT INTO token(token, user) VALUES(?, ?)", token, int(user))
	if err != nil {
		log.Println(err)
	}

	return FetchToken(token)
}

func (t Token) UserIfValid() User {
	return User(Int("user", "token", "id", strconv.Itoa(int(t)), 0))
}
