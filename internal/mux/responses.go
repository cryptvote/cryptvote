package mux

func Success() (interface{}, int) {
	return &struct {
		Success bool `json:"success"`
	}{
		Success: true,
	}, 200
}

func Error(statusCode int, message string) (interface{}, int) {
	return &struct {
		Error string `json:"error"`
	}{
		Error: message,
	}, statusCode
}

func InvalidRequest() (interface{}, int) {
	return Error(400, "invalidRequest")
}

func IncompleteRequest() (interface{}, int) {
	return Error(400, "incompleteRequest")
}

func Unauthorized() (interface{}, int) {
	return Error(401, "unauthorized")
}

func NotFound() (interface{}, int) {
	return Error(404, "notFound")
}

func InternalServerError() (interface{}, int) {
	return Error(500, "internalServerError")
}

func TooManyRequests() (interface{}, int) {
	return Error(429, "tooManyRequests")
}
