package mux

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gitlab.com/cryptvote/cryptvote/internal/db"
)

func fetchUser(r *http.Request) db.User {
	auth := r.Header.Get("Authorization")

	if auth == "" || !strings.HasPrefix(auth, "Token ") {
		return db.User(0)
	}

	return db.FetchToken(strings.TrimPrefix(auth, "Token ")).UserIfValid()
}

var rateLimiting map[string]int = make(map[string]int)

func init() {
	go func() {
		for {
			for k, _ := range rateLimiting {
				if rateLimiting[k] > 0 {
					rateLimiting[k] -= 5
				}
			}
			time.Sleep(time.Minute)
		}
	}()
}

func apiRoute(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Origin, Accept, Authorization")
	w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
	w.Header().Set("Vary", "Origin")
	w.Header().Set("Vary", "Access-Control-Request-Method")
	w.Header().Set("Vary", "Access-Control-Request-Headers")

	resp, status := x(r)

	jsonResp, err := json.Marshal(resp)
	if err != nil {
		status = 500
		resp = "internal server error"
	}

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(status)
	w.Write(jsonResp)
}

func x(r *http.Request) (interface{}, int) {
	var m map[string]ApiHandler

	switch r.Method {
	case "OPTIONS":
		return "ok", 200
	case "GET":
		m = gets
	case "POST":
		m = posts
	case "PUT":
		m = puts

		// Rate Limit PUT requests
		if rateLimiting[r.RemoteAddr] >= 10 {
			return TooManyRequests()
		} else {
			rateLimiting[r.RemoteAddr]++
		}
	case "DELETE":
		m = deletes
	}

	ep, ok := m[r.URL.Path]
	if !ok {

		return Error(404, "API handler not found")
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return InvalidRequest()
	}

	return ep(&ApiRequest{
		User:    fetchUser(r),
		Request: r,
		Body:    body,
	})
}
