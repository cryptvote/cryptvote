package mux

import (
	"net/http"

	"gitlab.com/cryptvote/cryptvote/internal/config"
)

var mux *http.ServeMux

func Mux() *http.ServeMux {
	if mux == nil {
		mux = &http.ServeMux{}

		if config.Config().HostFrontend {
			mux.HandleFunc("/", frontendRoute)
		}

		if config.Config().HostBackend {
			mux.HandleFunc("/api/", apiRoute)
		}
	}

	return mux
}
