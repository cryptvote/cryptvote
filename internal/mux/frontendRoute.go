package mux

import (
	"embed"
	"mime"
	"net/http"
	"path"
)

var frontend embed.FS

func SetFrontend(fe embed.FS) {
	frontend = fe
}

func frontendRoute(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", mime.TypeByExtension(path.Ext(r.URL.Path)))
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Cross-Origin-Resource-Policy", "cross-origin")

	file, err := frontend.ReadFile("frontend/build" + r.URL.Path)
	if err == nil {
		w.Write(file)
		return
	}

	file, err = frontend.ReadFile("frontend/build/index.html")
	if err == nil {
		w.Write(file)
		return
	}

	http.NotFound(w, r)
}
