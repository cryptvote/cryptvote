package mux

import (
	"errors"
	"net/http"

	"gitlab.com/cryptvote/cryptvote/internal/db"
)

type ApiRequest struct {
	User    db.User
	Request *http.Request
	Body    []byte
}

type ApiHandler func(req *ApiRequest) (interface{}, int)

var gets map[string]ApiHandler = make(map[string]ApiHandler)
var posts map[string]ApiHandler = make(map[string]ApiHandler)
var puts map[string]ApiHandler = make(map[string]ApiHandler)
var deletes map[string]ApiHandler = make(map[string]ApiHandler)

func regEp(path string, ep ApiHandler, m map[string]ApiHandler) error {
	if _, ok := m[path]; ok {
		return errors.New("path already exists")
	}
	m[path] = ep
	return nil
}

func Get(path string, ep ApiHandler) error {
	return regEp(path, ep, gets)
}

func Post(path string, ep ApiHandler) error {
	return regEp(path, ep, posts)
}

func Put(path string, ep ApiHandler) error {
	return regEp(path, ep, puts)
}

func Delete(path string, ep ApiHandler) error {
	return regEp(path, ep, deletes)
}
