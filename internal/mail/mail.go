package mail

import (
	"log"
	"net/mail"
	"net/smtp"
	"strconv"

	"gitlab.com/cryptvote/cryptvote/internal/config"
)

func Send(address string, subject string, text string) error {
	config := config.Config()

	auth := smtp.PlainAuth("", config.SmtpUsername, config.SmtpPassword, config.SmtpHost)
	from := mail.Address{Name: config.SmtpName, Address: config.SmtpUsername}

	headers := make(map[string]string)
	headers["From"] = from.String()
	headers["To"] = address
	headers["Subject"] = subject
	headers["Content-Type"] = "text/plain; charset=\"utf-8\""

	body := ""
	for k, v := range headers {
		body += k + ": " + v + "\r\n"
	}
	body += "\r\n" + text

	err := smtp.SendMail(config.SmtpHost+":"+strconv.Itoa(config.SmtpPort), auth, from.Address, []string{address}, []byte(body))
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}
