package config

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
)

type config struct {
	DbDriver             string `json:"dbDriver"`
	DbHost               string `json:"dbHost"`
	DbPort               int    `json:"dbPort"`
	DbUsername           string `json:"dbUsername"`
	DbPassword           string `json:"dbPassword"`
	DbName               string `json:"dbName"`
	SmtpHost             string `json:"smtpHost"`
	SmtpPort             int    `json:"smtpPort"`
	SmtpName             string `json:"smtpName"`
	SmtpUsername         string `json:"smtpUsername"`
	SmtpPassword         string `json:"smtpPassword"`
	RequireConfirmedMail bool   `json:"requireConfirmedMail"`
	ConfirmMailTitle     string `json:"confirmMailTitle"`
	ConfirmMailBody      string `json:"confirmMailBody"`
	RestrictEmailDomains bool   `json:"restrictEmailDomains"`
	AllowedEmailDomains  string `json:"allowedEmailDOmains"`
	ServerPort           int    `json:"serverPort"`
	HostFrontend         bool   `json:"hostFrontend"`
	HostBackend          bool   `json:"hostBackend"`
	InstanceName         string `json:"instanceName"`
	InstanceUrl          string `json:"instanceUrl"`
}

var c *config

func Config() *config {
	if c == nil {
		c = &config{}

		file, err := os.ReadFile("./config.json")
		if err == nil {
			// Read from config file if it exists
			err = json.Unmarshal(file, c)
			if err != nil {
				fmt.Println(err.Error())
			}
		} else {
			// Read from environment variables
			c.DbDriver = os.Getenv("DB_DRIVER")
			c.DbHost = os.Getenv("DB_HOST")
			c.DbPort, _ = strconv.Atoi(os.Getenv("DB_PORT"))
			c.DbUsername = os.Getenv("DB_USERNAME")
			c.DbPassword = os.Getenv("DB_PASSWORD")
			c.DbName = os.Getenv("DB_NAME")
			c.SmtpHost = os.Getenv("SMTP_HOST")
			c.SmtpPort, _ = strconv.Atoi(os.Getenv("SMTP_PORT"))
			c.SmtpUsername = os.Getenv("SMTP_USERNAME")
			c.SmtpName = os.Getenv("SMTP_NAME")
			c.SmtpPassword = os.Getenv("SMTP_PASSWORD")
			c.RequireConfirmedMail, _ = strconv.ParseBool(os.Getenv("REQUIRE_CONFIRMED_MAIL"))
			c.ConfirmMailTitle = os.Getenv("CONFIRM_MAIL_TITLE")
			c.ConfirmMailBody = os.Getenv("CONFIRM_MAIL_BODY")
			c.RestrictEmailDomains, _ = strconv.ParseBool(os.Getenv("RESTRICT_EMAIL_DOMAINS"))
			c.AllowedEmailDomains = os.Getenv("ALLOWED_EMAIL_DOMAINS")
			c.ServerPort, _ = strconv.Atoi(os.Getenv("SERVER_PORT"))
			c.HostFrontend, _ = strconv.ParseBool(os.Getenv("HOST_FRONTEND"))
			c.HostBackend, _ = strconv.ParseBool(os.Getenv("HOST_BACKEND"))
			c.InstanceName = os.Getenv("INSTANCE_NAME")
			c.InstanceUrl = os.Getenv("INSTANCE_URL")
		}
	}

	return c
}
