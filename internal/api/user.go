package api

import (
	"encoding/json"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/cryptvote/cryptvote/internal/config"
	"gitlab.com/cryptvote/cryptvote/internal/db"
	"gitlab.com/cryptvote/cryptvote/internal/mux"
	"golang.org/x/crypto/bcrypt"
)

func isUsernameTaken(username string) bool {
	if db.FetchUser(username) != db.User(0) {
		return true
	}
	return false
}

var usernameRegex = regexp.MustCompile("^[a-zA-Z0-9]{1,32}")

func isUsernameValid(username string) bool {
	return usernameRegex.MatchString(username)
}

func isEmailTaken(email string) bool {
	if db.Int("id", "user", "email", email, 0) != 0 {
		return true
	}
	return false
}

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func isEmailValid(email string) bool {
	return emailRegex.MatchString(email)
}

func isEmailDomainAllowed(email string) bool {
	if !config.Config().RestrictEmailDomains {
		return true
	}

	domain := strings.Split(email, "@")

	for _, v := range strings.Split(config.Config().AllowedEmailDomains, ",") {
		if v == domain[1] {
			return true
		}
	}

	return false
}

type PutUser struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
}

type PostUser struct {
	OldPassword string `json:"oldPassword"`
	NewPassword string `json:"newPassword"`
	NewEmail    string `json:"newEmail"`
}

type DeleteUser struct {
	Id       int
	Password string
}

type user struct {
	Id       int    `json:"id"`
	Username string `json:"username"`
}

func init() {
	mux.Get("/api/user", func(r *mux.ApiRequest) (interface{}, int) {
		// Check access
		if r.User == 0 {
			return mux.Unauthorized()
		}

		// Id argument
		id, err := strconv.Atoi(r.Request.URL.Query().Get("id"))
		if err != nil {
			// Return all users

			var users []user
			for _, u := range db.AllUsers() {
				users = append(users, user{
					Id:       int(u),
					Username: u.Username(),
				})
			}

			return users, 200
		}

		// Parse single user
		u := db.User(id)
		if u == 0 {
			return mux.NotFound()
		}

		return &struct {
			Username string `json:"username"`
		}{
			Username: u.Username(),
		}, 200
	})

	mux.Put("/api/user", func(r *mux.ApiRequest) (interface{}, int) {
		// Unmarshal request body
		req := PutUser{}
		if json.Unmarshal(r.Body, &req) != nil {
			return mux.InvalidRequest()
		}

		// Required fields
		if req.Username == "" || req.Password == "" || req.Email == "" {
			return mux.IncompleteRequest()
		}

		// Check if there is already a user with same username
		if isUsernameTaken(req.Username) {
			return mux.Error(400, "takenUsername")
		}

		// Check if username is valid
		if !isUsernameValid(req.Username) {
			return mux.Error(400, "invalidUsername")
		}

		// Check if there is already a user with same e-mail
		if isEmailTaken(req.Email) {
			return mux.Error(400, "takenEmail")
		}

		// Check if e-mail is valid
		if !isEmailValid(req.Email) {
			return mux.Error(400, "invalidEmail")
		}

		// Check if e-mail is allowed
		if !isEmailDomainAllowed(req.Email) {
			return mux.Error(400, "disallowedEmailDomain")
		}

		// Hash passwords
		passwordHash, err := bcrypt.GenerateFromPassword([]byte(req.Password), 10)
		if err != nil {
			return mux.InternalServerError()
		}

		db.CreateUser(req.Email, 0, req.Username, string(passwordHash))

		return mux.Success()
	})

	mux.Delete("/api/user", func(r *mux.ApiRequest) (interface{}, int) {
		// Unmarshal request body
		req := DeleteUser{}
		if json.Unmarshal(r.Body, &req) != nil {
			return mux.InvalidRequest()
		}

		// Check if account owner is sending request
		u := db.User(req.Id)

		if r.User != u {
			return mux.Unauthorized()
		}

		// Check password
		if bcrypt.CompareHashAndPassword([]byte(u.PasswordHash()), []byte(req.Password)) != nil {
			return mux.Unauthorized()
		}

		u.Delete()

		return mux.Success()
	})

	mux.Post("/api/user", func(r *mux.ApiRequest) (interface{}, int) {
		// Unmarshal request body
		req := PostUser{}
		if json.Unmarshal(r.Body, &req) != nil {
			return mux.InvalidRequest()
		}

		// Check if user is logged in
		if r.User == 0 {
			return mux.Unauthorized()
		}

		if req.NewPassword != "" {
			// Check password
			if bcrypt.CompareHashAndPassword([]byte(r.User.PasswordHash()), []byte(req.OldPassword)) != nil {
				return mux.Unauthorized()
			}

			// Change password
			passwordHash, err := bcrypt.GenerateFromPassword([]byte(req.NewPassword), 10)
			if err != nil {
				return mux.Error(200, "invalidCredentials")
			}

			r.User.SetPasswordHash(string(passwordHash))
		}

		// Change e-mail
		if req.NewEmail != "" {
			r.User.SetEmail(req.NewEmail)
			r.User.SetEmailConfirmed(false)
		}

		return mux.Success()
	})
}
