package api

import (
	"encoding/json"
	"log"
	"math/rand"

	"gitlab.com/cryptvote/cryptvote/internal/config"
	"gitlab.com/cryptvote/cryptvote/internal/db"
	"gitlab.com/cryptvote/cryptvote/internal/mux"
)

type PutVote struct {
	Poll           string `json:"poll"`
	EncryptedValue string `json:"encryptedValue"`
	EncryptedProof string `json:"encryptedProof"`
}

type vote struct {
	EncryptedValue string `json:"encryptedValue"`
	EncryptedProof string `json:"encryptedProof"`
}

type voted struct {
	User db.User
	Poll db.Poll
}

var voteQueues map[db.Poll][]vote = make(map[db.Poll][]vote)
var votedQueues map[db.Poll][]voted = make(map[db.Poll][]voted)

func queue(poll db.Poll, user db.User, encryptedValue string, encryptedProof string) {
	// Append to queues
	voteQueues[poll] = append(voteQueues[poll], vote{EncryptedValue: encryptedValue, EncryptedProof: encryptedProof})
	votedQueues[poll] = append(votedQueues[poll], voted{User: user})

	// Shuffle queues
	rand.Shuffle(len(voteQueues[poll]), func(i, j int) {
		voteQueues[poll][i], voteQueues[poll][j] = voteQueues[poll][j], voteQueues[poll][i]
	})
	rand.Shuffle(len(votedQueues[poll]), func(i, j int) {
		votedQueues[poll][i], votedQueues[poll][j] = votedQueues[poll][j], votedQueues[poll][i]
	})

	// Determine queue size needed to write to database
	x := 2
	if len(poll.InvitedUsers())-len(voteQueues[poll]) == 3 {
		x = 3
	}

	// Write to database
	if len(voteQueues[poll]) >= x {
		ProcessQueue(poll)
	}
}

func ProcessQueue(poll db.Poll) {
	// Write to database
	for _, v := range voteQueues[poll] {
		db.CreateVote(poll, v.EncryptedProof, v.EncryptedValue)
	}
	for _, v := range votedQueues[poll] {
		v.User.CastVote(poll)
	}

	// Empty queues
	vote := make([]vote, 0)
	voted := make([]voted, 0)

	voteQueues[poll] = vote
	votedQueues[poll] = voted
}

func init() {
	mux.Get("/api/vote", func(r *mux.ApiRequest) (interface{}, int) {
		// Check access
		if r.User == 0 || (config.Config().RequireConfirmedMail && !r.User.EmailConfirmed()) {
			return mux.Unauthorized()
		}

		// Id argument
		id := r.Request.URL.Query().Get("id")
		if id == "" {
			return mux.IncompleteRequest()
		}

		// Parse poll
		poll := db.FetchPoll(id)
		if poll.EncryptedQuestion() == "" {
			return mux.NotFound()
		}

		// Check if user can vote
		if !r.User.IsInvited(poll) {
			return mux.Unauthorized()
		}

		// Check if poll is expired
		if !poll.Expired() {
			log.Println(poll.Expiry())
			return mux.Error(400, "notOverYet")
		}

		// Write queue to database
		ProcessQueue(poll)

		var result []vote

		for _, v := range poll.Votes() {
			result = append(result, vote{EncryptedProof: v.EncryptedProof(), EncryptedValue: v.EncryptedValue()})
		}

		return result, 200
	})

	mux.Put("/api/vote", func(r *mux.ApiRequest) (interface{}, int) {
		// Unmarshal request
		req := PutVote{}
		if json.Unmarshal(r.Body, &req) != nil {
			return mux.InvalidRequest()
		}

		// Check access
		if r.User == 0 || (config.Config().RequireConfirmedMail && !r.User.EmailConfirmed()) {
			return mux.Unauthorized()
		}

		// Parse poll
		poll := db.FetchPoll(req.Poll)
		if poll.EncryptedQuestion() == "" {
			return mux.NotFound()
		}

		// Check if user can vote
		if !r.User.IsInvited(poll) {
			return mux.Unauthorized()
		}

		// Check if poll is expired
		if poll.Expired() {
			return mux.Error(400, "pollExpired")
		}

		// Check if user has already voted
		if r.User.HasVoted(poll) {
			return mux.Error(400, "alreadyVoted")
		}

		for _, v := range votedQueues[poll] {
			if v.User == r.User {
				return mux.Error(400, "alreadyVoted")
			}
		}

		// Add to vote queue
		queue(poll, r.User, req.EncryptedValue, req.EncryptedProof)

		return mux.Success()
	})
}
