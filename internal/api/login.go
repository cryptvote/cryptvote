package api

import (
	"encoding/json"
	"strings"
	"time"

	"gitlab.com/cryptvote/cryptvote/internal/db"
	"gitlab.com/cryptvote/cryptvote/internal/mux"
	"golang.org/x/crypto/bcrypt"
)

type PostLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

var rateLimiting map[string]int = make(map[string]int)

func init() {

	go func() {
		for k, _ := range rateLimiting {
			if rateLimiting[k] > 0 {
				rateLimiting[k]--
			}
		}
		time.Sleep(time.Minute)
	}()

	mux.Post("/api/login", func(r *mux.ApiRequest) (interface{}, int) {
		req := PostLogin{}
		if json.Unmarshal(r.Body, &req) != nil {
			return mux.InvalidRequest()
		}

		// Rate limiting
		if rateLimiting[strings.ToLower(req.Username)] >= 5 {
			return mux.TooManyRequests()
		} else {
			rateLimiting[strings.ToLower(req.Username)]++
		}

		// Parse user
		u := db.FetchUser(req.Username)

		// Check credentials
		if int(u) == 0 || bcrypt.CompareHashAndPassword([]byte(u.PasswordHash()), []byte(req.Password)) != nil {
			return mux.Error(200, "invalidCredentials")
		}

		return &struct {
			Id             int    `json:"id"`
			Token          string `json:"token"`
			Email          string `json:"email"`
			EmailConfirmed bool   `json:"emailConfirmed"`
			Username       string `json:"username"`
			DisplayName    string `json:"displayName"`
		}{
			Id:             int(u),
			Token:          db.CreateToken(u).Token(),
			Email:          u.Email(),
			EmailConfirmed: u.EmailConfirmed(),
			Username:       u.Username(),
			DisplayName:    u.DisplayName(),
		}, 200
	})
}
