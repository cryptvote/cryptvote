package api

import (
	"encoding/json"
	"time"

	"gitlab.com/cryptvote/cryptvote/internal/config"
	"gitlab.com/cryptvote/cryptvote/internal/db"
	mux "gitlab.com/cryptvote/cryptvote/internal/mux"
)

type PutPoll struct {
	EncryptedQuestion string `json:"encryptedQuestion"`
	EncryptedProof    string `json:"encryptedProof"`
	Expiry            int64  `json:"expiry"`
	Users             []int  `json:"users"`
}

func init() {
	mux.Put("/api/poll", func(r *mux.ApiRequest) (interface{}, int) {
		// Unmarshal request body
		req := PutPoll{}
		if json.Unmarshal(r.Body, &req) != nil {
			return mux.InvalidRequest()
		}

		// Check access
		if r.User == 0 || (config.Config().RequireConfirmedMail && !r.User.EmailConfirmed()) {
			return mux.Unauthorized()
		}

		// Check if is not empty
		if req.EncryptedQuestion == "" || req.EncryptedProof == "" {
			return mux.IncompleteRequest()
		}

		// Check expiry date
		expiry := time.Unix(req.Expiry, 0)
		if expiry.Before(time.Now()) {
			return mux.Error(400, "pastExpiry")
		}

		// Check if users exist
		for _, u := range req.Users {
			if db.User(u).Username() == "" {
				return mux.Error(400, "invalidUser")
			}
		}

		return &struct {
			Id string `json:"id"`
		}{
			Id: db.CreatePoll(r.User, req.Expiry, req.EncryptedQuestion, req.EncryptedProof, req.Users).Poll(),
		}, 200
	})

	mux.Get("/api/poll", func(r *mux.ApiRequest) (interface{}, int) {
		// Check access
		if r.User == 0 || (config.Config().RequireConfirmedMail && !r.User.EmailConfirmed()) {
			return mux.Unauthorized()
		}

		// Id argument
		id := r.Request.URL.Query().Get("id")
		if id == "" {
			return mux.IncompleteRequest()
		}

		// Parse poll
		poll := db.FetchPoll(id)
		if poll.EncryptedQuestion() == "" {
			return mux.NotFound()
		}

		// Check if user can vote
		if !r.User.IsInvited(poll) {
			return mux.Unauthorized()
		}

		// Check if user has voted
		voted := false

		if r.User.HasVoted(poll) {
			voted = true
		}

		for _, v := range votedQueues[poll] {
			if v.User == r.User {
				voted = true
				break
			}
		}

		return &struct {
			EncryptedQuestion string `json:"encryptedQuestion"`
			EncryptedProof    string `json:"encryptedProof"`
			Expiry            int64  `json:"expiry"`
			Author            int    `json:"author"`
			Voted             bool   `json:"voted"`
		}{
			EncryptedQuestion: poll.EncryptedQuestion(),
			EncryptedProof:    poll.EncryptedProof(),
			Author:            int(poll.Author()),
			Expiry:            poll.Expiry().Unix(),
			Voted:             voted,
		}, 200
	})
}
