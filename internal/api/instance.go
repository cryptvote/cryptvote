package api

import (
	"gitlab.com/cryptvote/cryptvote/internal/config"
	"gitlab.com/cryptvote/cryptvote/internal/mux"
)

func init() {
	mux.Get("/api/instance", func(r *mux.ApiRequest) (interface{}, int) {
		url := "http://" + r.Request.Host

		if r.Request.TLS != nil {
			url = "https://" + r.Request.Host
		}

		if config.Config().InstanceUrl != "" {
			url = config.Config().InstanceUrl
		}

		return &struct {
			Url                  string `json:"url"`
			Name                 string `json:"name"`
			CryptVote            bool   `json:"cryptvote"`
			RequireConfirmedMail bool   `json:"requireConfirmedMail"`
		}{
			Url:                  url,
			Name:                 config.Config().InstanceName,
			CryptVote:            true,
			RequireConfirmedMail: config.Config().RequireConfirmedMail,
		}, 200
	})
}
