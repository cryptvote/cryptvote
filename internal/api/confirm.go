package api

import (
	"encoding/json"
	"math/rand"
	"time"

	"gitlab.com/cryptvote/cryptvote/internal/config"
	"gitlab.com/cryptvote/cryptvote/internal/db"
	"gitlab.com/cryptvote/cryptvote/internal/mail"
	"gitlab.com/cryptvote/cryptvote/internal/mux"
)

type PostConfirm struct {
	Code string `json:"code"`
}

func generateConfirmationCode() string {
	letters := []rune("0123456789")

	s := make([]rune, 6)
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

var getRateLimit map[db.User]bool = make(map[db.User]bool)
var postRateLimit map[db.User]int = make(map[db.User]int)

func init() {

	go func() {
		for k, _ := range postRateLimit {
			if postRateLimit[k] > 0 {
				postRateLimit[k]--
			}
		}
		time.Sleep(time.Minute)
	}()

	mux.Get("/api/confirm", func(r *mux.ApiRequest) (interface{}, int) {
		// Check if user is logged in
		if r.User == 0 {
			return mux.Unauthorized()
		}

		// Check if e-mail is already confirmed
		if r.User.EmailConfirmed() {
			return mux.Error(400, "alreadyConfirmed")
		}

		// Rate limiting
		if getRateLimit[r.User] {
			return mux.TooManyRequests()
		}

		getRateLimit[r.User] = true

		go func() {
			time.Sleep(30 * time.Second)
			getRateLimit[r.User] = false
		}()

		// Generate & send code
		code := generateConfirmationCode()

		if mail.Send(r.User.Email(), config.Config().ConfirmMailTitle, config.Config().ConfirmMailBody+code) != nil {
			return mux.InternalServerError()
		}

		r.User.SetConfirmationCode(code)

		return mux.Success()
	})

	mux.Post("/api/confirm", func(r *mux.ApiRequest) (interface{}, int) {
		// Check if user is logged in
		if r.User == 0 {
			return mux.Unauthorized()
		}

		// Check if e-mail is already confirmed
		if r.User.EmailConfirmed() {
			return mux.Error(400, "alreadyConfirmed")
		}

		// Rate limiting
		if postRateLimit[r.User] >= 5 {
			return mux.TooManyRequests()
		} else {
			postRateLimit[r.User]++
		}

		req := PostConfirm{}
		if json.Unmarshal(r.Body, &req) != nil {
			return mux.InvalidRequest()
		}

		// Check if confirmation code is valid
		if r.User.ConfirmationCode() != req.Code {
			return mux.Error(400, "invalidCode")
		}

		r.User.SetEmailConfirmed(true)
		return mux.Success()

	})
}
