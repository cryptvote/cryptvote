module gitlab.com/cryptvote/cryptvote

go 1.18

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/mattn/go-sqlite3 v1.14.12
    golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
)

