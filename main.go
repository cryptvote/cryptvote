package main

import (
	"embed"
	"log"
	"net/http"
	"strconv"

	_ "gitlab.com/cryptvote/cryptvote/internal/api"
	"gitlab.com/cryptvote/cryptvote/internal/config"
	"gitlab.com/cryptvote/cryptvote/internal/db"
	"gitlab.com/cryptvote/cryptvote/internal/mux"
)

//go:embed frontend/build
var frontend embed.FS

func main() {
	db.Db()
	defer db.Close()

	mux.SetFrontend(frontend)

	// Webserver
	server := http.Server{
		Addr:    ":" + strconv.Itoa(config.Config().ServerPort),
		Handler: mux.Mux(),
	}

	err := server.ListenAndServe()
	if err != nil {
		log.Println(err.Error())
	}
}
